# Code2PDF

This program will use ChromeDriver to nicely format your source code using given arguments. This is intended to use for projects where a requirement
is to have the code be published via pdf (I'm talking about education of course). It will firstly install chromedriver and use it to render the code
requested through the `--src` argument, and use [highlight js](https://highlightjs.org/) to apply syntax highlighting, and 
[highlight js line numbers](https://github.com/wcoder/highlightjs-line-numbers.js/) to add line numbers to the code. 

[Selenium](https://www.selenium.dev/), the web driver, will automatically install `chromedriver` for you so you won't need to do anything tricky on
your end. 

## Usage

This program requires you to have installed [.NET Core 8](https://dotnet.microsoft.com/en-us/download/dotnet/8.0), and
works on Linux. It should, in theory, work on Windows and Mac but I haven't tested it as of now.

### Install

For personal use, run `dotnet tool install -g code2pdf` in favourite terminal (if your using Windows, I think this will
only work in PowerShell), then follow the [command line instructions](#command-line)

### Development

This program requires you to have installed [.NET Core 8](https://dotnet.microsoft.com/en-us/download/dotnet/8.0). Once 
installed, follow the next steps

```sh
git clone https://gitlab.com/jedsaxon/code2pdf
cd code2pdf
dotnet build
```

### Command Line

```
--src,     -s  |  Source Code Directory. It will recursively include all files within it
--out,     -o  |  Output PDF File Path. Once the document has been generated, it will save it as a .pdf file to this path
--help,    -h  |  Shows this page
--template -t  |  Select your own custom template file for code to be displayed on. See https://gitlab.com/jedsaxon/code2pdf for details about using custom templates
```

### Examples

The following command will take the code from `~/my-project`, and output a `.pdf` file in `~/my-fancy-code.pdf`. Please note that the program 
requires you at least use the `--src` and `--out` parameters.
```sh
# Normal use case
code2pdf --src ~/my-project --out ~/my-fancy-code.pdf  
code2pdf -s ~/my-project -o ~/my-fancy-code.pdf  

# With a template
code2pdf --src ~/my-project --out ~/my-fancy-code.pdf --template ~/a-cool-template.html
code2pdf -s ~/my-project -o ~/my-fancy-code.pdf -t ~/a-cool-template.html

# Using dotnet (for development purposes)
dotnet run --project Code2PDF -s ~/my-project -o ~/my-fancy-code.pdf -t ~/a-cool-template.html
```

### Templates

The included template file is included in `/ConsoleApplication/Resources/template.html` and will be used if the `-t` argument is not passed in. If 
you would like to use a custom template (perhaps with custom theme), you must include the following:

1. Include `hljs`, `hljs-line-numbers`:

```html
<head>
    <!-- ... -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/default.min.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/highlight.min.js'></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/highlight.js@11.9.0/styles/github.min.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/highlightjs-line-numbers.js/2.8.0/highlightjs-line-numbers.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/highlightjs-line-numbers.js@2.8.0/dist/highlightjs-line-numbers.min.js'></script>
    <!-- ... -->
</head>
```

2. Add an element with a the id `code`, preferably containing nothing inside. Your code will be inserted into there

```html
<section id='code'></section>
```

3. Add some custom styling (optional)

```html
<style>
    .code-box {
        /* Container for <pre> */
    }

    pre {
        /* Container for <code> */
    }

    code {
        /* Actual Text */
    }

    .hljs-ln-numbers {
        /* Line Numbers */
    }

    .hljs-ln-code {
        /* Highlighted Code Elements */
    }
</style>
```

4. Then, of course, use the template command as shown in [Examples](#examples)
