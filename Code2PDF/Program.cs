﻿using Code2PDF;
using Code2PDF.Exceptions;

var helper = new ConsoleHelper();

// HEADER   
helper.PrintAsciiArt();
ArgParser.ProgramOptions options;

try
{
    options = ArgParser.ParseArgsForOptions(args);
}
catch (ArgumentException e)
{
    helper.Print(e.Message, true, ConsoleColor.Red);
    helper.Print("This program converts your code into a .pdf document. It takes in a --src or -s document (source code directory), and converts all supported code documents into the --out (output file) .pdf file\n--src,  -s  |  Source Code Directory. It will recursively include all files within it\n--out,  -o  |  Output PDF File Path. Once the document has been generated, it will save it as a .pdf file to this path\n--help, -h  |  Shows this page");

    return;
}

if (options.IncludesHelp)
{
    helper.Print("""
                 This program converts your code into a .pdf document. It takes in a --src or -s document (source code directory), and 
                 converts all supported code documents into the --out (output file) .pdf file
                 --src,     -s  |  Source Code Directory. It will recursively include all files within it
                 --out,     -o  |  Output PDF File Path. Once the document has been generated, it will save it as a .pdf file to this path
                 --help,    -h  |  Shows this page
                 --template -t  |  Select your own custom template file for code to be displayed on. See https://gitlab.com/jedsaxon/code2pdf for details about using custom templates
                 """);
    return;
}

helper.Print("Downloading ChromeDriver (will take a long time on first use)");
var reader = new CodeReader(options.SourceDir ?? throw new NullReferenceException("Source directory was null. Try passing in the --src argument. Use --help for details."));
var files = reader.GetAllFiles().ToArray();

// Performing initial checks
var tempFileManager = new TemporaryFileManager();
HtmlTemplate html;
try 
{
    html = new HtmlTemplate(options, tempFileManager);

    if (options.TemplateFile is not null) 
        await html.LoadTemplate(options.TemplateFile);
    else 
        await html.LoadTemplate();
}
catch(NoTemplateFileLoadedException)
{
    helper.Print("Unable to load the template file. Please ensure that it exists, and is a .HTML file", true, ConsoleColor.Red);
    return;
}
catch(Exception e)
{
    helper.Print($"Unknown error: {e}");
    return;
}

helper.Print($"Inserting all {files.Length.ToString()} files into website");
for (var i = 0; i < files.Length; i++)
{
    var file = files[i];
    var code = await reader.GetCodeFromFile(file);

    try 
    {
        html.AddCodeBlock(file.Name, file.Extension, code);
    }
    catch(NoTemplateFileLoadedException)
    {
        helper.Print($"Error: No template was loaded", true, ConsoleColor.Red);
    }
    catch(NoCodeBlockExtension)
    {
        helper.Print($"Error: The template does NOT have a <section> element with the id=\"code\".", true, ConsoleColor.Red);
    }

    helper.Print($"Formatting {(i / (float)files.Length * 100):f0}%: {file.FullName}");
}

helper.Print("Executing hljs...");
try 
{ 
    html.RunHljs();
}
catch (OpenQA.Selenium.JavaScriptException e)
{
    helper.Print("A javascript error has occured while executing hljs. The program will continue, but there will be no syntax highlighting. Are you connected to the internet?", true, ConsoleColor.Red);
}

html.PrintTo(options.OutputFile!.FullName);

helper.Print("Finished!");
Thread.Sleep(1000);
html.Close();
