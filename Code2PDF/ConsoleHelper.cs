namespace Code2PDF;

public class ConsoleHelper
{
    public void Print(string msg, bool newLine = true, ConsoleColor? colour = null) 
    {
        if (colour is not null)
            Console.ForegroundColor = (ConsoleColor)colour;
        
        if (newLine) Console.WriteLine(msg);
        else Console.Write(msg);

        Console.ResetColor();
    }

    public void PrintAsciiArt()
    {
        Console.ForegroundColor = ConsoleColor.DarkRed;
        Console.WriteLine("""
                                           _      ___            _  __ 
                                          | |    |__ \          | |/ _|
                              ___ ___   __| | ___   ) |_ __   __| | |_ 
                             / __/ _ \ / _` |/ _ \ / /| '_ \ / _` |  _|
                            | (_| (_) | (_| |  __// /_| |_) | (_| | |  
                             \___\___/ \__,_|\___|____| .__/ \__,_|_|  
                                                      | |              
                                                      |_|              
                          """);
        Console.ResetColor();
    }
}
