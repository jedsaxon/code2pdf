using System.Text;

namespace Code2PDF;

/// <summary>
/// Responsible for finding all relevant files, within a given source directory. 
/// </summary>
public class CodeReader
{
    public DirectoryInfo SoruceDir { get; private set; }

    private string[] IgnoreDirectories { get; init; } = new[] 
    {
        "bin", "obj"
    };

    private string[] AllowedFileExtensions { get; init; } = new [] 
    {
        ".html", ".css", ".js", ".xml", ".json", ".yaml", ".yml", ".csv", ".txt", ".md", ".c", ".cpp", ".h", ".hpp", ".cs", ".java", ".php", ".py", ".rb", ".pl", ".sql", ".sh", ".ps1", ".bat", ".cmd", ".asm", ".ini", ".cfg", ".conf"
    };

    public CodeReader(DirectoryInfo sourceDir) 
    {
        SoruceDir = sourceDir;
    }

    public IEnumerable<FileInfo> GetAllFiles() => GetAllFiles(SoruceDir);

    public async Task<string> GetCodeFromFile(FileInfo file)
    {
        if (!file.Exists) throw new FileNotFoundException();

        var code = new StringBuilder();
        using (var stream = file.OpenText())
        {
            var s = string.Empty;
            while ((s = await stream.ReadLineAsync()) is not null)
                code.AppendLine(s);
        }

        return code.ToString();
    }

    /// <summary>
    /// Gets all code files within the SourceDir directory. It will disclude any hidden files/directories
    /// </summary>
    private IEnumerable<FileInfo> GetAllFiles(DirectoryInfo root)
    {
        List<FileInfo> files = new List<FileInfo>();
        DirectoryInfo[] subDirectories = root.GetDirectories();

        // Recursively loop through all directories
        for (var i = 0; i < subDirectories.Length; i++)
        {
            var subdir = subDirectories[i];

            var subdirName = subdir.Name;
            if (subdirName.StartsWith(".") || DirectoryShouldBeIgnored(subdir)) continue;

            files.AddRange(GetAllFiles(subdir));
        }

        // Add all files present in the directory, and return it to the previous caller
        FileInfo[] unprocessedFiles = root.GetFiles();
        for (var i = 0; i < unprocessedFiles.Length; i++)
        {
            if (!FileShouldBeIgnored(unprocessedFiles[i]))
                files.Add(unprocessedFiles[i]);
        }

        return files;
    }

    private bool DirectoryShouldBeIgnored(DirectoryInfo directory)
    {
        for (var j = 0; j < IgnoreDirectories.Length; j++) 
        {
            var ignoredDirectoryName = IgnoreDirectories[j];
            if (ignoredDirectoryName.Equals(directory.Name)) return true;
        }

        return false;
    }

    private bool FileShouldBeIgnored(FileInfo file)
    {
        return !AllowedFileExtensions.Contains(file.Extension);
    }
}
