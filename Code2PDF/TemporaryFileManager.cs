namespace Code2PDF;

/// <summary>
/// Helper class to assist with managing temporary files and directories. When this object is destroyed or out of scope, any temporary objects will be destroyed
/// </summary>
public class TemporaryFileManager : IDisposable
{
    private readonly DirectoryInfo _temporaryPath;

    public TemporaryFileManager()
    {
        _temporaryPath = new DirectoryInfo(Path.GetTempPath());   
        if (!_temporaryPath.Exists) _temporaryPath.Create();
    }

    /// <summary>
    /// Will generate a temporary file for use anywhere, with the given extension. As per usual, it will be deleted when the object instance is out of scope or destroyed
    /// </summary>
    public async Task<FileInfo> CreateTemporaryFile(string extension) 
    {
        FileInfo? tempFile = null;

        // Loop until new file can be made
        while (tempFile is null || tempFile.Exists)
            tempFile = new FileInfo(Path.Join(_temporaryPath.FullName, CreateTemporaryName() + extension));
        
        await tempFile.Create().DisposeAsync();
        return tempFile;
    }

    public DirectoryInfo CreateTemporaryDirectory()
    {
        DirectoryInfo? tempDir = null;
        // Loop until a new directory that does not exist can be made
        while (tempDir is null || tempDir.Exists) 
            tempDir = new DirectoryInfo(Path.Join(_temporaryPath.FullName, CreateTemporaryName()));
        
        tempDir.Create();
        return tempDir;
    }

    private string CreateTemporaryName()
    {
        return Guid.NewGuid().ToString();
    }

    public void Dispose()
    {
        if (_temporaryPath.Exists) _temporaryPath.Delete();
        GC.SuppressFinalize(this);
    }
}
