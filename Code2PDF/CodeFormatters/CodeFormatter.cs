using System.Text;

namespace Code2PDF.CodeFormatters;

/// <summary>
/// This class exists to account for how different code syntax and provides a modular way to implement such. For example, you could create a formatter which converts a .csv file into a HTML table, or to convert .html syntax into something that could be displayed in a tag without it being displayed as actual HTML syntax
/// </summary>
public abstract class CodeFormatter
{
    /// <summary>
    /// This flag is active when the formatted code uses it's own HTML blocks. By default, it will use <pre><code class="lanugage-{...}">...</code></pre>. However, when this is TRUE, that initial HTML will not be included in the output of the program and should be implemented manually inside the Format implementation.
    /// </summary>
    public abstract bool UsesCustomHtml { get; } 

    /// <summary>
    /// Formats the code provided, and will be Appended into the StringBuilder, before being returned
    /// </summary>
    public abstract StringBuilder Format(StringBuilder builder, string code);
}
