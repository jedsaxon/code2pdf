using System.Text;
using System.Web;

namespace Code2PDF.CodeFormatters;

public class GenericCodeFormatter : CodeFormatter
{
    public override bool UsesCustomHtml => false;

    public override StringBuilder Format(StringBuilder builder, string code)
    {
        return builder.Append(HttpUtility.HtmlEncode(code)); 
    }
}
