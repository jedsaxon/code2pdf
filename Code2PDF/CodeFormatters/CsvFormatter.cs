using System.Text;
using System.Web;

namespace Code2PDF.CodeFormatters;

public class CsvFormatter : CodeFormatter
{
    public override bool UsesCustomHtml => true;
    private const int MAX_LINES = 40;

    public override StringBuilder Format(StringBuilder builder, string code)
    {
        builder.Append("<table class=\"table table-striped\">");

        var lines = code.Split('\n');
        for (var i = 0; i < lines.Length; i++)
        {
            if (i > MAX_LINES) break; // this can be put into the for loop but I'm to lazy to

            var line = lines[i].Trim();

            if (string.IsNullOrWhiteSpace(line)) continue;

            builder.Append("<tr>");
            var fields = line.Split(',');
            for (var j = 0; j < fields.Length; j++)
            {
                var field = fields[j].Trim();
                var htmlField = HttpUtility.UrlEncode(field);

                // Add line number to the first column
                if (i == 0 && j == 0)
                    builder.Append("<th>").Append("#").Append("</th>");
                else if (j == 0) 
                    builder.Append("<td>").Append(i).Append("</td>");

                // Then add the CSV data
                if (i == 0) // Header
                    builder.Append("<th>").Append(htmlField).Append("</th>");    
                else
                    builder.Append("<td>").Append(htmlField).Append("</td>");

            }
            builder.Append("</tr>");
        }

        builder.Append("</table>");

        if (lines.Length > MAX_LINES)
            builder.Append("<div class=\"alert alert-info my-2 mb-2\">There are ").Append(lines.Length - MAX_LINES).Append(" lines remaining in this file</div>");

        return builder;
    }
}
