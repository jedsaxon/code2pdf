using System.Text;
using Newtonsoft.Json.Linq;

namespace Code2PDF.CodeFormatters;

public class JsonFormatter : CodeFormatter
{
    public override bool UsesCustomHtml => true;

    public override StringBuilder Format(StringBuilder builder, string code)
    {
        JObject jsonObject;
        try 
        {
            jsonObject = JObject.Parse(code);
        }
        catch
        {
            builder.Append("<div class=\"alert alert-danger\">JSON object is invalid</div>");
            return builder;
        }

        builder.Append("<table class=\"table table-striped\">");
        builder.Append("<tr><th>Property Name</th><th>Data Type</th></tr>");
       
        foreach (var property in jsonObject.Properties())
        {
            builder.Append("<tr>");
            builder.Append("<td>").Append(property.Name).Append("</td>");
            builder.Append("<td>").Append(property.Value.Type).Append("</td>");
            builder.Append("</tr>");
        }
        
        return builder.Append("</table>");
    }
}
