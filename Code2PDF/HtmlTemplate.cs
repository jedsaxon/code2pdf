using System.Text;
using Code2PDF.CodeFormatters;
using Code2PDF.Exceptions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Code2PDF;

public class HtmlTemplate : IDisposable
{
    /// <summary> 
    /// The location where all code blocks will be added to in order to be rendered as a website 
    /// </summary>
    private FileInfo? HtmlFile { get; set; }
    private IWebElement? CodeSection { get; set; }

    private readonly TemporaryFileManager _tempFileManager;
    private readonly WebDriver _webDriver;
    private readonly IJavaScriptExecutor _jsExec;
    private readonly PrintOptions _options = new()
    {
        Orientation = PrintOrientation.Landscape
    };

    /// <summary>
    /// Maps a file extension (.cs, .html, .js, etc) to a formatter. 
    /// </summary>
    private readonly Dictionary<string, CodeFormatter> _extensionFormaterMap = new Dictionary<string, CodeFormatter>()
    {
        { ".csv", new CsvFormatter() },
        { ".json", new JsonFormatter() }
    };
    private readonly CodeFormatter _defaultFormatter = new GenericCodeFormatter();

    public HtmlTemplate(ArgParser.ProgramOptions options, TemporaryFileManager tempFileManager)
    {
        var chromeOptions = new ChromeOptions();
        chromeOptions.AddArgument("headless");

        _tempFileManager = tempFileManager;
        _webDriver = new ChromeDriver(chromeOptions);
        _jsExec = _webDriver;
    }

    /// <summary>
    /// Loads the basic HTML template from a file (as plain text) that the code will sit on top of. It then copies the contents of the file into a temporary file which will be edited to include the `hljs` library. You will NEED to an id="code" somewheres, where the code will be stored before printed
    /// </summary>
    public async Task LoadTemplate(FileInfo file)
    { 
        var builder = new StringBuilder();

        using (var reader = file.OpenText())
            while (await reader.ReadLineAsync() is { } line)
                builder.AppendLine(line);

        await LoadTemplate(builder.ToString());
    }

    /// <summary>
    /// Loads the default template packaged by the program
    /// </summary>
    public async Task LoadTemplate()
    {
        var file = new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "template.html"));
        if (!file.Exists)
            throw new FileNotFoundException("Could not find the default template.html file packaged by the project. Please define one using --template, and see --help for more information");
        await LoadTemplate(file);
    }

    /// <summary>
    /// Loads the basic HTML template from the provided string that the code will sit on top of. You will NEED to add a section tag with an id="code" which is where the code will be stored before printed
    /// </summary>
    public async Task LoadTemplate(string html)
    {
        HtmlFile = await _tempFileManager.CreateTemporaryFile(".html");
        await File.WriteAllTextAsync(HtmlFile.FullName, html);
        _webDriver.Navigate().GoToUrl(Path.Join("file://", HtmlFile.FullName));

        // Make sure there is a section with the id "code"
        try 
        {
            CodeSection = _webDriver.FindElement(By.CssSelector("#code"));
        }
        catch
        {
            throw new NoCodeBlockExtension();
        }

        _jsExec.ExecuteScript("let code = \"\"");
    }

    public void AddCodeBlock(string fileName, string extension, string code)
    {
        if (!Constants.ExtensionToClassName.TryGetValue(extension, out var className)) {
            className = string.Empty;
        }
        
        if (CodeSection is null) throw new NoCodeBlockExtension();

        var formatter = SelectFormatterFor(extension);

        var html = new StringBuilder();
        html.Append("<section>");
        html.Append("<h2>").Append(fileName).Append("</h2>");

        if (!formatter.UsesCustomHtml)
        {
            html.Append("<pre>");
            html.Append("<code class=\"").Append(className).Append("\">");
        }
        else
        {
            formatter.Format(html, code);
        }

        if (!formatter.UsesCustomHtml)
        {
            formatter.Format(html, code).Append("</pre></code>");
        }

        html.Append("</section>");

        const string command = $"arguments[0].innerHTML += arguments[1]";
        _jsExec.ExecuteScript(command, CodeSection, html.ToString());
    }

    public void RunHljs()
    {
        _jsExec.ExecuteScript("hljs.highlightAll()");
        _jsExec.ExecuteScript("hljs.initLineNumbersOnLoad();");
    }

    private CodeFormatter SelectFormatterFor(string extension)
    {
        return _extensionFormaterMap.GetValueOrDefault(extension, _defaultFormatter);
    }

    public void PrintTo(string fileLocation)
    {
        var printedDoc = _webDriver.Print(_options);
        var pdf = new FileInfo(fileLocation);
        if (pdf.Exists) pdf.Delete();
        printedDoc.SaveAsFile(pdf.FullName);
    }

    public void Close()
    {
        _webDriver.Quit();
    }

    public void Dispose()
    {
        Close();
        GC.SuppressFinalize(this);
    }
}
