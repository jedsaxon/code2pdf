namespace Code2PDF;

public static class ArgParser
{
    private static readonly string[] ARG_HELP = ["--help", "-h"];
    private static readonly string[] ARG_SOURCE_DIR = ["--src", "-s"];
    private static readonly string[] ARG_OUTPUT_FILE = ["--out", "-o"];
    private static readonly string[] ARG_TEMPLATE_FILE = ["--template", "-t"];

    private enum ArgState
    {
        None,
        Help,
        SourceDir,
        OutputFile,
        Template,
    }

    public class ProgramOptions
    {
        public bool IncludesHelp { get; set; }
        public DirectoryInfo? SourceDir { get; set; } = null;
        public FileInfo? OutputFile { get; set; } = null;
        public FileInfo? TemplateFile { get; set; } = null;
    }

    public static ProgramOptions ParseArgsForOptions(string[] args)
    {
        var state = ArgState.None;
        var options = new ProgramOptions();

        if (args.Length == 0 || args.Contains("--help") || args.Contains("-h")) 
            return HelpOnly();
        
        for (var i = 0; i < args.Length; i++)
        {
            switch (state)
            {
                case ArgState.None: 
                    state = FindArgType(args[i]);
                    break;
                case ArgState.Help: // Include this here for fallback
                    options.IncludesHelp = true;
                    state = ArgState.None;
                    break;
                case ArgState.SourceDir:
                    options.SourceDir = new DirectoryInfo(args[i]);
                    if (!options.SourceDir.Exists) 
                        throw new ArgumentException($"Invalid Argument --src -s: the directory does not exist.");
                    state = ArgState.None;
                    break;
                case ArgState.OutputFile:
                    options.OutputFile = new FileInfo(args[i]);
                    state = ArgState.None;
                    break;
                case ArgState.Template:
                    var file = new FileInfo(args[i]);
                    if (!file.Exists) {
                        throw new ArgumentException("The template file does not exist. Please ensure it is created and exists");
                    }
                    options.TemplateFile = file;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(args), "This exception should not happen; Invalid arg state detected while parsing arguments list.");
            }
        }

        // Just return options if the user typed in --help; the user of this function should know to check for this. Imagine if I were using rust?
        if (options.IncludesHelp) return options;

        if (options.OutputFile is null) throw new ArgumentException("You must include an output file with the --out flag. Use --help for options.");
        if (options.SourceDir is null) throw new ArgumentException("You must include a source directory with the --src flag. Use --help for options.");

        return options;
    }

    private static ProgramOptions HelpOnly()
    {
        return new ProgramOptions { IncludesHelp = true };
    }

    private static ArgState FindArgType(string arg)
    {
        if (ARG_HELP.Contains(arg)) return ArgState.Help;
        if (ARG_OUTPUT_FILE.Contains(arg)) return ArgState.OutputFile;
        if (ARG_SOURCE_DIR.Contains(arg)) return ArgState.SourceDir;
        if (ARG_TEMPLATE_FILE.Contains(arg)) return ArgState.Template;
        throw new ArgumentException($"Unexpected Argument {arg}. Type --help or -h for available options.");
    }
}
